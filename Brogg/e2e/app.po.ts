import { browser, element, by } from 'protractor';

export class BroggPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('gg-root h1')).getText();
  }
}
