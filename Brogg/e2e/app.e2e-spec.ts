import { BroggPage } from './app.po';

describe('brogg App', function() {
  let page: BroggPage;

  beforeEach(() => {
    page = new BroggPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('gg works!');
  });
});
