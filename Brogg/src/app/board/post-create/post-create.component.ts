import { Component, OnInit, AfterViewInit, OnDestroy, Input, NgZone } from '@angular/core';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {Post} from "../../models/post"
import {PostsService} from "../../shared/posts.service";
declare var tinymce: any;

@Component({
  selector: 'gg-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent implements OnInit, OnDestroy, AfterViewInit {
  postForm: FormGroup;
  success: boolean = false;
  constructor(private postsService : PostsService, private zone: NgZone) { }
  text;
  post: Post;
  editor;
  ngOnInit() {
    this.postForm = new FormGroup({
      'title': new FormControl('', Validators.required),
      'description': new FormControl('', Validators.required),
      'body': new FormControl(''),
    });

    tinymce.init({
        selector: '#' + "post-create-editor",
        plugins: "lists",
        setup: editor => {
          this.editor = editor}
      });
  }

  ngAfterViewInit(){
  }

  onSubmit(_post: Post){
    if(this.postForm.valid == true){
      this.success = false;
      this.post = _post;
      this.text = tinymce.activeEditor.getContent();
      console.log(this.post);
      console.log(this.text);
      this.post.body = this.text;
      this.postsService.create(this.post)
            .subscribe(data => this.onSubmitSuccess(data));
    }
  }

  private onSubmitSuccess(data){
    this.success = true;
    console.log('success');
  }

  ngOnDestroy(){
    tinymce.remove(this.editor);
  }
}
