import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import {FormsModule, NgForm} from "@angular/forms";
import {Post} from '../../models/post';
import {Comment} from '../../models/comment';
import {PostsService} from '../../shared/posts.service';
import {CommentService} from '../../shared/comment.service';
import {AuthenticationService} from '../../shared/authentication.service';

declare var tinymce: any;

@Component({
  selector: 'gg-full-post',
  templateUrl: './full-post.component.html',
  styleUrls: ['./full-post.component.css']
})
export class FullPostComponent implements OnInit {
  post: Post;
  comments: Comment[];
  private routeSubscription: Subscription;

  constructor(private postsService: PostsService, private route: ActivatedRoute, private commentService : CommentService, private authService : AuthenticationService) { }
  isAdmin:boolean = false;
  isLoggedIn: boolean = false;

  ngOnInit() {
    this.routeSubscription = this.route.params
      .subscribe((params: any)=>{
        let postId = params['id'];
        this.postsService.getById(postId)
            .subscribe((data)=>{
              this.post = data;
              this.loadComments();
        });
    });
    
    this.isAdmin = this.authService.IsAdminSync();
    this.isLoggedIn = this.authService.IsLoggedInSync();
  }

  loadComments(){
    this.commentService.GetComments(this.post._id).subscribe((data)=>{
                  this.comments = data;
                  console.log("comments are here" + data);
              });
  }
  onNewComment(){
   this.loadComments();
  }
}
