export { PostComponent } from './post/post.component';
export { PostEditComponent } from './post-edit/post-edit.component';
export { PostCreateComponent } from './post-create/post-create.component';
export { BoardComponent } from './board.component';
export { FullPostComponent } from './full-post/full-post.component';
