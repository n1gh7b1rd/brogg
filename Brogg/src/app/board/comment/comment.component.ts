import { Component, OnInit, Input } from '@angular/core';
import {CommentService} from '../../shared/comment.service';
import {FormsModule, NgForm} from "@angular/forms";
import {Comment} from '../../models/comment';

@Component({
  selector: 'gg-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  @Input() postId;
  constructor(private commentService: CommentService) { }
  @Input() commentText;
  @Input() commentAuthor;
  ngOnInit() {
  }

  onSubmit(){
    let comment : Comment;
    comment.postId = this.postId;
    comment.text = this.commentText;
    this.commentService.AddComment(comment).subscribe((res)=>{
      console.log("added comment");
    });
  }
}
