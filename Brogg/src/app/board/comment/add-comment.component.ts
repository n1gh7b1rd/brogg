import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import {CommentService} from '../../shared/comment.service';
import {FormsModule, NgForm} from "@angular/forms";
import {Comment} from '../../models/comment';

@Component({
  selector: 'gg-add-comment',
  templateUrl: './add-comment.component.html',
  styleUrls: ['./add-comment.component.css']
})
export class AddCommentComponent implements OnInit {

  @Input() postId;
  @Output() onAdd = new EventEmitter();

  text: string;
  constructor(private commentService: CommentService) { }

  ngOnInit() {
  }

  AddComment(data){
    console.log("in add comment");
    console.log(this.postId);
    var comment:any = {};
    comment.postId = this.postId;
    comment.text = data.text;
    this.commentService.AddComment(comment).subscribe((res)=>{
      this.text = '';
      this.onAdd.emit(res);
    });
  }
}
