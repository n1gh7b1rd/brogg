import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import {FormsModule, NgForm} from "@angular/forms";
import {Post} from '../../models/post';
import {PostsService} from '../../shared/posts.service';
declare var tinymce: any;

@Component({
  selector: 'gg-post-edit',
  templateUrl: './post-edit.component.html',
  styleUrls: ['./post-edit.component.css']
})
export class PostEditComponent implements OnInit, OnDestroy  {
  success: boolean = false;
  title: any;
  description: any;
  body: any;
  postId: any;
  editor;
  private subscription: Subscription;

  constructor(private postsService: PostsService, private route: ActivatedRoute) {
    
   }

  ngOnInit() {
    this.subscription = this.route.params
      .subscribe((params: any)=>{
        this.postId = params['id'];
        this.postsService.getById(this.postId)
            .subscribe((data)=>{
              this.loadPost(data);
            });
    });

     tinymce.init({
        selector: '#' + "post-editor",
        plugins: "lists",
        setup: editor => {
          this.editor = editor;
      }
      });
  }

  private loadPost(data){
    console.log(data.title);
    this.title = data.title;
    this.description = data.description;
    this.body = data.body;

     if(this.body!=undefined){
          this.editor.setContent(this.body);
      }
  }

  onSubmit(post: Post){
      this.success = false;
      post._id = this.postId;
      post.body = tinymce.activeEditor.getContent();
      this.postsService.edit(post).subscribe((res)=>{
       this.success = true;
       console.log('success');
      })
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
    tinymce.remove(this.editor);
  }

}
