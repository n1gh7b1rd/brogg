import { Component, OnInit, OnDestroy } from '@angular/core';
import {PostsService} from '../shared/posts.service'
import { Post } from '../models/post'
import {AuthenticationService} from '../shared/authentication.service';
import { Subscription } from "rxjs/Rx";

@Component({
  selector: 'gg-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit, OnDestroy {
  posts: Post[] = [];
  isAdmin;
  isLoggedIn;
  private isAdminSub: Subscription;
  private isLoggedInSub: Subscription;


  constructor(private postsService : PostsService, private authService : AuthenticationService) { }
   

  ngOnInit() {
    this.loadPosts();
    this.isAdmin= this.authService.IsAdmin().subscribe((isAdmin)=>{this.isAdmin = isAdmin});;
    this.isLoggedInSub = this.authService.IsLoggedIn().subscribe((isLoggedIn)=>{this.isLoggedIn = isLoggedIn});
    this.authService.TriggerAdminCheck();
    this.authService.TriggerIsLoggedInCheck();
  }

  onPostDeleted(event){
    this.loadPosts();
  }

  private loadPosts(){
    this.postsService.getAll().subscribe(data => this.posts = data);
  }

   ngOnDestroy(){
    
  }
}
