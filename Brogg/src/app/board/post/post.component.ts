import { Component, OnInit, OnDestroy, Input, EventEmitter, Output, OnChanges } from '@angular/core';
import { Post } from '../../models/post';
import { Comment } from '../../models/comment'
import {CommentComponent} from '../comment/comment.component';CommentService
import {PostsService} from '../../shared/posts.service';
import {CommentService} from '../../shared/comment.service';
import {AuthenticationService} from '..//../shared/authentication.service';
import { Subscription } from "rxjs/Rx";

@Component({
  selector: 'gg-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit, OnDestroy {
  
  isAdmin: boolean = false;
  isLoggedIn: boolean=false;
  comments: Comment[];
  @Input() post: Post;
  constructor(private postsService: PostsService, private commentService : CommentService, private authService : AuthenticationService) { }

  ngOnInit() {
  //  this.commentService.GetComments(this.post._id).subscribe((comments)=>{
  //    this.comments = comments;
  //  });
   
   this.isAdmin= this.authService.IsLoggedInSync();
   this.isLoggedIn = this.authService.IsLoggedInSync();
  }
  ngOnChanges(){
    
  }
  ngOnDestroy(){
 
  }
}
