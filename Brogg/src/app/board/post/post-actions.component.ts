import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {PostsService} from '../../shared/posts.service';
import { Subscription } from "rxjs/Rx";

@Component({
  selector: 'gg-post-actions',
  templateUrl: './post-actions.component.html',
  styleUrls: ['./post-actions.component.css']
})
export class PostActionsComponent implements OnInit {
  @Output() onDeleted = new EventEmitter();
  @Input() postId;
  @Input() isAdmin = false;
  @Input() isLoggedIn = false;
  constructor(private postsService: PostsService) { }

  ngOnInit() {
  }

  deleted(event){
    this.postsService.delete(this.postId)
      .subscribe(data=>this.onDeleted.emit(this.postId));    
  }
}
