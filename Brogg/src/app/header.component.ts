import { Component, OnInit, OnDestroy } from '@angular/core';
import {AuthenticationService} from './shared/authentication.service';
import { Subscription } from "rxjs/Rx";

@Component({
  selector: 'gg-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  private isAdmin: boolean = false;
  private isAdminSub: Subscription;
  private isLoggedIn: boolean = false;
  private isLoggedInSub: Subscription;
  constructor(private authService : AuthenticationService) { }

  ngOnInit() {
    this.isAdminSub = this.authService.IsAdmin()
      .subscribe((isAdmin)=>{this.isAdmin = isAdmin});
    
    this.isLoggedInSub = this.authService.IsLoggedIn().subscribe((isLoggedIn)=>{this.isLoggedIn = isLoggedIn});
  }

  ngOnDestroy(){
    this.isAdminSub.unsubscribe();
    this.isLoggedInSub.unsubscribe();
  }
}
