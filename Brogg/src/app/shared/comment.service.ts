import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import {AppSettings} from '../app.settings';
import 'rxjs/Rx';
import { Observable } from "rxjs/Rx";
import {Comment} from '../models/comment';

@Injectable()
export class CommentService {

  constructor(private http: Http) {
    this.headers = new Headers();
    this.postHeaders = new Headers();
    this.postHeaders.append('Content-Type', 'application/json');
   }
  private headers: Headers;
  private postHeaders: Headers;

  public AddComment(comment: Comment){

    const _comment = JSON.stringify(comment);
    
    return this.http.post(AppSettings.API_ENDPOINT + "comment/add", _comment, {headers: this.postHeaders,  withCredentials: true})
      .catch(this.handleError);
  }

  public GetComments(postId){
    return this.http.get(AppSettings.API_ENDPOINT + "comment/getByPostId/" + postId, {headers: this.headers, withCredentials:true})
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  private handleError(error: any){
    console.log(error);
    return Observable.throw(error);
  }
}
