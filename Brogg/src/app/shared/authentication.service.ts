import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams } from '@angular/http';
import { User } from '../models/user';
import {AppSettings} from '../app.settings';
import 'rxjs/Rx';
import { Observable, Subject } from "rxjs/Rx";
@Injectable()
export class AuthenticationService {

  constructor(private http: Http) { this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
   }
  private headers: Headers;
  isAdminSubject = new Subject<boolean>();
  isLoggedIn = new Subject<boolean>();

  public IsAdmin() : Observable<boolean> {
    this.isAdminSubject.next(false);
    return this.isAdminSubject.asObservable();
  }
  public IsLoggedIn() : Observable<boolean>{
    this.isLoggedIn.next(false);
    return this.isLoggedIn.asObservable();
  }

  public TriggerIsLoggedInCheck(){
    let isUserLoggedIn = this.IsLoggedInSync();
    
    if(isUserLoggedIn){
      this.isLoggedIn.next(true);
    }
    else{
      this.isLoggedIn.next(false);
    }
    return isUserLoggedIn;
  }

  public IsLoggedInSync(){
    return this.GetUsername() !=undefined;
  }

  public IsAdminSync(){
    return  localStorage.getItem("role") == "Admin"? true: false;
  }

  public TriggerAdminCheck(){
    var result = this.IsAdminSync();
    if(result){
      this.isAdminSubject.next(true);
    }
    else{
      this.isAdminSubject.next(false);
    }

    return result;
  }

  public GetUsername(){
    return localStorage.getItem("user");
  }

  public login(user: User){
    const _user = JSON.stringify(user);
    let result =  this.http.post(AppSettings.API_ENDPOINT + "login",_user, {headers: this.headers, withCredentials: true})
      .catch(this.handleError);
    return result;
  }

  public register(user: User){

    var body: any =  {};
    body.username = user.username;
    body.password = user.password;
    return this.http.post(AppSettings.API_ENDPOINT + "register", body, {headers: this.headers})
      .catch(this.handleError);
  }

  public logout(){
    return this.http.get(AppSettings.API_ENDPOINT + "logout", {withCredentials: true})
    .catch(this.handleError);
  }

  public userInfo(){
    return this.http.get(AppSettings.API_ENDPOINT + "user/info", {withCredentials: true})
    .map((response: Response) => response.json())
    .catch(this.handleError);
  }

  private handleError(error: any){
    console.log(error);
    return Observable.throw(error);
  }
}
