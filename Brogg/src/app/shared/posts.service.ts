import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Post } from '../models/post'
import {AppSettings} from '../app.settings';
import 'rxjs/Rx';
import { Observable } from "rxjs/Rx";

@Injectable()
export class PostsService {

  constructor(private http: Http) {
    this.headers = new Headers();
    this.postHeaders = new Headers();
    this.postHeaders.append('Content-Type', 'application/json');
   }
  private headers: Headers;
  private postHeaders: Headers;

  //C
  public create(post: Post){
    const _post = JSON.stringify(post);
    
    return this.http.post(AppSettings.API_ENDPOINT + "post/create", _post, {headers: this.postHeaders,  withCredentials: true})
      //.map((response: Response) => response.json())
      .catch(this.handleError);
  }

  //R
  public getAll(){
    return this.http.get(AppSettings.API_ENDPOINT + "post/all", {headers: this.headers, withCredentials: true})
      .map((response: Response) => response.json());
  }

  public getById(id: any){
    return this.http.get(AppSettings.API_ENDPOINT + "post/getById/" + id, {withCredentials:true})
      .map((response: Response) => response.json())
      .catch(this.handleError);;
    }

  //U
  public edit(post: Post){
     const _post = JSON.stringify(post);
    let result =  this.http.post(AppSettings.API_ENDPOINT + "post/edit", _post, 
          {headers: this.postHeaders,  
           withCredentials: true,
           })
      .catch(this.handleError);

      return result;
  }
  
  //D
  public delete(id: any){
    const _id = JSON.stringify({_id: id});
    return this.http.post(AppSettings.API_ENDPOINT + "post/delete", _id, {headers: this.postHeaders, withCredentials: true})
      .catch(this.handleError);
  }

  private handleError(error: any){
    console.log(error);
    return Observable.throw(error);
  }
  
}
