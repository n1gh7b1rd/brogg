export class AppSettings {
//    public static get API_ENDPOINT(): string { return 'http://localhost:3000/'; }
//    public static get STATIC_FILES(): string { return 'http://localhost:3000/public/';}
   public static get API_ENDPOINT(): string { return 'https://brogg-api.herokuapp.com/'; }
   public static get STATIC_FILES(): string { return 'https://brogg-api.herokuapp.com/public/';}

}