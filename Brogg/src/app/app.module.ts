import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header.component';
import { BoardComponent } from './board/board.component';
import {PostsService} from './shared/posts.service';
import {AuthenticationService} from './shared/authentication.service';
import {CommentService} from './shared/comment.service';
import { PostComponent } from './board/post/post.component';
import { routing } from './app.routing';
import { PostEditComponent } from './board/post-edit/post-edit.component';
import { PostCreateComponent } from './board/post-create/post-create.component';
import { HeaderdropdownDirective } from './headerdropdown.directive';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './login/register.component';
import { LogoutComponent } from './login/logout.component';
import { AfterHeadComponent } from './design/after-head.component';
import { CommentComponent } from './board/comment/comment.component';
import { FullPostComponent } from './board/full-post/full-post.component';
import { PostActionsComponent } from './board/post/post-actions.component';
import { AddCommentComponent } from './board/comment/add-comment.component';
import { SideShortInfoComponent } from './design/side-short-info.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BoardComponent,
    PostComponent,
    PostEditComponent,
    PostCreateComponent,
    HeaderdropdownDirective,
    LoginComponent,
    RegisterComponent,
    LogoutComponent,
    AfterHeadComponent,
    CommentComponent,
    FullPostComponent,
    PostActionsComponent,
    AddCommentComponent,
    SideShortInfoComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    routing
  ],
  providers: [PostsService, AuthenticationService, CommentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
