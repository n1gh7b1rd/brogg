import { Component, OnInit, trigger, state, transition, animate, style } from '@angular/core';
import {AppSettings} from '../app.settings';

@Component({
  selector: 'gg-after-head',
  templateUrl: './after-head.component.html',
  styleUrls: ['./after-head.component.css'],
  animations:[
    trigger('divState', [
      state('void', style({
        opacity: 0,
      })),
      state('shown', style({
        opacity: 1,
      })),
      transition('void => *', animate(4000)),
    ])
  ]
})
export class AfterHeadComponent implements OnInit {

  constructor() { }

  logoPath: string = "";
  ngOnInit() {
    this.logoPath =  AppSettings.STATIC_FILES + "broggblog-logo-cut-double.png";
    console.log(AppSettings.STATIC_FILES);
    console.log(this.logoPath);

  }

}
