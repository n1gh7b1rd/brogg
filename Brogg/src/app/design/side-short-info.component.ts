import { Component, OnInit, trigger, state, style, transition, animate } from '@angular/core';
import {AppSettings} from '../app.settings';

@Component({
  selector: 'gg-side-short-info',
  templateUrl: './side-short-info.component.html',
  styleUrls: ['./side-short-info.component.css'],
  animations:[
    trigger('divState', [
      state('void', style({
        opacity: 0,
        transform: 'translateX(+400px)',
      })),
      state('shown', style({
        opacity: 1,
        transform: 'translateX(0)',
      })),
      transition('void => *', animate(3000)),
    ])
  ]
})
export class SideShortInfoComponent implements OnInit {
  avatarPath;
  state = "normal";
  constructor() { }

  ngOnInit() {
    this.avatarPath = AppSettings.STATIC_FILES + "avatar-blaze-cut.png";
  }

}
