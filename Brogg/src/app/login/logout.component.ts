import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../shared/authentication.service';

@Component({
  selector: 'gg-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  logoutSuccess: boolean = false;
  constructor(private authService: AuthenticationService) { }

  ngOnInit() {
    this.authService.logout()
      .subscribe(()=>{
        this.logoutSuccess = true;
         localStorage.removeItem("user");
         localStorage.removeItem("role");
         this.triggerLogoutChecks();
    });;
  }

  triggerLogoutChecks(){
    this.authService.TriggerAdminCheck();
    this.authService.TriggerIsLoggedInCheck();
  }
}
