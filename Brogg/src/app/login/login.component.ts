import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../shared/authentication.service';
import { User } from '../models/user';
import {FormsModule, NgForm} from "@angular/forms";

@Component({
  selector: 'gg-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthenticationService) { }
  username;
  password;
  success: boolean;

  ngOnInit() {

  }

  onSubmit(user: User){
    this.success = false;
    this.authService.login(user).subscribe((res)=>{
       this.success = true;
       this.authService.userInfo().subscribe((res)=>{
         localStorage.setItem("user", res.username);
         localStorage.setItem("role", res.role);
         this.triggerLoginChecks();
       });
       console.log('login successful');
      });

  }

  triggerLoginChecks(){
    this.authService.TriggerAdminCheck();
    this.authService.TriggerIsLoggedInCheck();
  }

}
