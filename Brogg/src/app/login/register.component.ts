import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../shared/authentication.service';
import { User } from '../models/user';
import {FormsModule, NgForm} from "@angular/forms";

@Component({
  selector: 'gg-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  success: boolean;
  username;
  password;
  constructor(private authService: AuthenticationService) { }

  ngOnInit() {
  }

  onSubmit(user: User){
    this.success = false;
    console.log(user);
    this.authService.register(user).subscribe((res)=>{
       this.success = true;
       console.log('registration successful');
      });
  }
}
