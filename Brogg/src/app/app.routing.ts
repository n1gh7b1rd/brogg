import { Routes, RouterModule } from '@angular/router';
import {BoardComponent, PostEditComponent, PostCreateComponent, FullPostComponent} from './board'
import {LoginComponent, RegisterComponent, LogoutComponent } from './login';

const APP_ROUTES: Routes = [
    {path: '', redirectTo: 'posts', pathMatch: 'full'},
    {path: 'posts', component: BoardComponent},
    {path: 'post-edit/:id', component: PostEditComponent},
    {path: 'post-create', component: PostCreateComponent},
    {path: 'post-full/:id', component: FullPostComponent},
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent},
    {path: 'logout', component: LogoutComponent},
];

export const routing = RouterModule.forRoot(APP_ROUTES);